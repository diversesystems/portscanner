import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class PortScanner {

    static String ip = "127.0.0.1"; // localhost
    static ConcurrentHashMap<Integer, Boolean> ports = new ConcurrentHashMap<>();

    public static void main(String[] args) {

        int tSize = Runtime.getRuntime().availableProcessors();

        int pSize = 65535 / tSize;

        //handles extra so each thread works on the same number of ports even if there are left overs
        Thread[] tArr = new Thread[tSize + 1];

        for (int i = 0; i <= tSize; i++) {
            try {
                System.out.println("Scanning ports from : " + ((pSize * i) + 1) + " to " + (pSize * (i + 1)));

                tArr[i] = new Thread(new RunPS(i, pSize));
                tArr[i].start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        for (Thread t : tArr) {
            try {
                t.join();
            } catch (Exception e) {

            }
        }

        for (Map.Entry<Integer, Boolean> e : ports.entrySet()) {
            System.out.println(e.getKey() + " " + e.getValue());
        }
    }

    static class RunPS implements Runnable {

        int start;
        int portDiv;

        public RunPS(int s, int p) {
            start = s;
            portDiv = p;
        }

        @Override
        public void run() {
            int limit = (portDiv * (start + 1));

            if(limit > 65535)
            {
                limit = 65535;
                System.out.println("Only 65535 ports on a computer, setting new upper limit");
            }
            
            for (int j = ((portDiv * start) + 1); j <= limit; j++) {
                try {
                    Socket sock = new Socket();
                    sock.connect(new InetSocketAddress(ip, j));
                    sock.close();
                    ports.put(j, true);
                } catch (Exception e) {
                    ports.put(j, false);
                    System.out.println(j);
                }

            }
        }
    }
}